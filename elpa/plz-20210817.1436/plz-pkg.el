;;; Generated package description from plz.el  -*- no-byte-compile: t -*-
(define-package "plz" "20210817.1436" "HTTP library" '((emacs "26.3")) :authors '(("Adam Porter" . "adam@alphapapa.net")) :maintainer '("Adam Porter" . "adam@alphapapa.net") :keywords '("comm" "network" "http") :url "https://github.com/alphapapa/plz.el")
