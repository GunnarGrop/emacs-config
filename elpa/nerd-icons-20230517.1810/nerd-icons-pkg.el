(define-package "nerd-icons" "20230517.1810" "Emacs Nerd Font Icons Library"
  '((emacs "24.3"))
  :commit "39df68f66f204b1a0680203a2b1483ed4a746987" :authors
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/rainstormstudio/nerd-icons.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
