(define-package "helm" "20230517.519" "Helm is an Emacs incremental and narrowing framework"
  '((helm-core "3.9.0")
    (popup "0.5.3"))
  :commit "b734d776bb16470819ad45758e2af29b3db9ef31" :authors
  '(("Thierry Volpiatto" . "thievol@posteo.net"))
  :maintainers
  '(("Thierry Volpiatto" . "thievol@posteo.net"))
  :maintainer
  '("Thierry Volpiatto" . "thievol@posteo.net")
  :url "https://emacs-helm.github.io/helm/")
;; Local Variables:
;; no-byte-compile: t
;; End:
