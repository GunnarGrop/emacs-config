(define-package "php-mode" "20230423.1446" "Major mode for editing PHP code"
  '((emacs "26.1"))
  :commit "37b2b883300e396d250d49c8d0f5a3abe6effa7f" :authors
  '(("Eric James Michael Ritz"))
  :maintainers
  '(("USAMI Kenta" . "tadsan@zonu.me"))
  :maintainer
  '("USAMI Kenta" . "tadsan@zonu.me")
  :keywords
  '("languages" "php")
  :url "https://github.com/emacs-php/php-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
