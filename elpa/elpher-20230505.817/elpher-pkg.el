(define-package "elpher" "20230505.817" "A friendly gopher and gemini client"
  '((emacs "27.1"))
  :commit "81f2883614c303184116449ec3583ef5c136ec2f" :authors
  '(("Tim Vaughan" . "plugd@thelambdalab.xyz"))
  :maintainer
  '("Tim Vaughan" . "plugd@thelambdalab.xyz")
  :keywords
  '("comm" "gopher" "gemini")
  :url "https://thelambdalab.xyz/elpher")
;; Local Variables:
;; no-byte-compile: t
;; End:
