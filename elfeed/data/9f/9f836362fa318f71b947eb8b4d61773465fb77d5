<p>So you&rsquo;re starting a new website, and you open the first CSS file. What style do
you use? Well, you hate indenting with spaces passionately. You know tabs are
right because they&rsquo;re literally made for this, and they&rsquo;re only one byte, and
these god damn spaces people with their bloody spacebars&hellip;</p>
<p>Shut up and use spaces. That&rsquo;s how CSS is written<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup>. And you, mister web
programmer, coming out of your shell and dipping your toes into the world of
Real Programming, writing your first Golang program: use tabs, jerk. There&rsquo;s
only one principle that matters in coding style: don&rsquo;t rock the boat. Just do
whatever the most common thing is in the language you&rsquo;re working in. Write your
commit messages the same way as everyone else, too. Then shut up and get back to
work. This hill isn&rsquo;t worth dying on.</p>
<p>If you&rsquo;re working on someone else&rsquo;s project, this goes double. Don&rsquo;t get snippy
about their coding style. Just follow their style guide, and if there isn&rsquo;t one,
just make your code look like the code around it. It&rsquo;s none of your goddamn
business how they choose to style their code.</p>
<p>Shut up and get back to work.</p>
<p>Ranting aside, seriously - which style guide you use doesn&rsquo;t matter nearly as
much as using one. Just pick the one which is most popular or which is already
in use by your peers and roll with it.</p>
<div style="margin-bottom: 5rem"></div>
<p>&hellip;though since I&rsquo;m talking about style anyway, take a look at this:</p>
<div class="highlight"><pre class="chroma"><code class="language-c" data-lang="c"><span class="k">struct</span> <span class="n">wlr_surface</span> <span class="o">*</span><span class="nf">wlr_surface_surface_at</span><span class="p">(</span><span class="k">struct</span> <span class="n">wlr_surface</span> <span class="o">*</span><span class="n">surface</span><span class="p">,</span>
                                           <span class="kt">double</span> <span class="n">sx</span><span class="p">,</span> <span class="kt">double</span> <span class="n">sy</span><span class="p">,</span>
                                           <span class="kt">double</span> <span class="o">*</span><span class="n">sub_x</span><span class="p">,</span> <span class="kt">double</span> <span class="o">*</span><span class="n">sub_y</span><span class="p">)</span> <span class="p">{</span>
    <span class="c1">// Do stuff
</span><span class="c1"></span><span class="p">}</span>
</code></pre></div><p>There&rsquo;s a lot of stupid crap which ends up in style guides, but this is by far
the worst. Look at all that wasted whitespace! There&rsquo;s no room to write your
parameters on the right, and you end up with 3 lines where you could have two.
And you have to mix spaces and tabs! God dammit! This is how you should do it:</p>
<div class="highlight"><pre class="chroma"><code class="language-c" data-lang="c"><span class="k">struct</span> <span class="n">wlr_surface</span> <span class="o">*</span><span class="nf">wlr_surface_surface_at</span><span class="p">(</span><span class="k">struct</span> <span class="n">wlr_surface</span> <span class="o">*</span><span class="n">surface</span><span class="p">,</span>
        <span class="kt">double</span> <span class="n">sx</span><span class="p">,</span> <span class="kt">double</span> <span class="n">sy</span><span class="p">,</span> <span class="kt">double</span> <span class="o">*</span><span class="n">sub_x</span><span class="p">,</span> <span class="kt">double</span> <span class="o">*</span><span class="n">sub_y</span><span class="p">)</span> <span class="p">{</span>
    <span class="c1">// Do stuff
</span><span class="c1"></span><span class="p">}</span>
</code></pre></div><p>Note the extra indent to distinguish the parameters from the body and the
missing garish hellscape of whitespace. If you do this in your codebase, I&rsquo;m not
going to argue with you about it, but I am going to have to talk to my therapist
about it.</p>
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p>For the record, tabs are objectively better. Does that mean I&rsquo;m going to write my JavaScript with tabs? Hell no! <a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</section>
