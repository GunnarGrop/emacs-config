<p>Today&rsquo;s federated revolution is led by ActivityPub, leading to the rise of
services like Mastodon, PeerTube, PixelFed, and more. These new technologies
have a particular approach to federation, which is coloring perceptions on what
it actually means for a system to be federated at all. Today&rsquo;s post will explain
how <a href="https://en.wikipedia.org/wiki/Internet_Relay_Chat">Internet Relay Chat</a>
(IRC), a technology first introduced in the late 1980&rsquo;s, does federation
differently, and why.</p>
<p>As IRC has aged, many users today have only ever used a few networks, such as
Liberachat (or Freenode, up until several weeks ago), which use a particular IRC
model which does not, at first glance, appear to utilize federation. After all,
everyone types &ldquo;irc.libera.chat&rdquo; into their client and they all end up on the
same network and in the same namespace. However, this domain name is backed by a
round-robin resolver which will connect you to any of <a href="https://netsplit.de/servers/?net=Libera.Chat">several dozen
servers</a>, which are connected to each other<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup> and exchange messages on
behalf of the users who reside on each. This is why we call them IRC <em>networks</em>
— each is composed of a network of servers that work together.</p>
<p>But why can&rsquo;t I send messages to users on <a href="https://www.oftc.net">OFTC</a> from my Libera Chat session?
Well, IRC networks are federated, but they are typically a <em>closed</em> federation,
such that each network forms a discrete graph of servers, not interconnected
with any of the others. In ActivityPub terms, imagine a version of Mastodon
where, instead of automatically federating with new instances, server operators
whitelisted each one, forming a closed graph of connected instances. Organize
these servers under a single named entity (&ldquo;Mastonet&rdquo; or something), and the
result is an &ldquo;ActivityPub network&rdquo; which operates in the same sense as a typical
&ldquo;IRC network&rdquo;.</p>
<p>In contrast to Mastodon&rsquo;s open federation, allowing any server to peer with any
others without prior agreement between their operators, most IRC networks are
closed. The network&rsquo;s servers may have independent operators, but they operate
together under a common agreement, rather than the laissez-faire approach
typical of<sup id="fnref:2"><a href="#fn:2" class="footnote-ref" role="doc-noteref">2</a></sup> ActivityPub servers. The exact organizational and governance
models vary, but many of these networks have discrete teams of staff which
serve as moderators<sup id="fnref:3"><a href="#fn:3" class="footnote-ref" role="doc-noteref">3</a></sup>, often unrelated to the people responsible for the
servers. The social system can be designed independently of the technology.</p>
<p>Among IRC networks, there are degrees of openness. Libera Chat, the largest
network, is run by a single governing organization, using servers donated by
(and in the possession of) independent sponsors. Many smaller networks are be
run on as few as one server, and some larger networks (particularly older ones)
are run by many independent operators acting like more of a cooperative.
<a href="http://efnet.org">EFnet</a>, the oldest network, is run in this manner — you
can even <a href="http://www.efnet.org/?module=docs&amp;doc=16">apply to become an operator</a> yourself.</p>
<p>We can see from this that the idea of federation is flexible, allowing us to
build a variety of social and operational structures. There&rsquo;s no single right
answer — approaches like IRC are able to balance many different benefits
and drawbacks of their approach, such as balancing a reduced level of user
mobility with a stronger approach to moderation and abuse reduction, while
simultaneously enjoying the cost and scalability benefits of a federated design.
Other federations, like Matrix, email, and Usenet, have their own set of
tradeoffs. What unifies them is the ability to scale to a large size without
expensive infrastructure, under the social models which best suit their users'
needs, without a centralizing capital motive.</p>
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p>Each server is not necessarily connected to each other server, by the way.  Messages can be relayed from one server to another repeatedly to reach the intended destination. This provides IRC with a greater degree of scalability when compared to ActivityPub, where each server must communicate directly with the servers whose users it needs to reach. It also makes IRC more vulnerable to outages partitioning the network; we call these incidents &ldquo;netsplits&rdquo;. <a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:2" role="doc-endnote">
<p>Typical, but not universal. <a href="#fnref:2" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:3" role="doc-endnote">
<p>There are two classes of moderators on IRC: oppers and ops. The former is responsible for the network, and mainly concerns themselves with matters of spam, user registration, settling disputes, and supporting ops. The ops are responsible for specific channels (spaces for discussion) and can define and enforce further rules at their discretion, within any limits imposed by the host network. <a href="#fnref:3" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</section>
