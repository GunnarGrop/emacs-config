<p>I&rsquo;m pleased to announce that the final pieces have fallen into place for
<a href="https://www.mercurial-scm.org/">Mercurial</a> support on
<a href="https://sourcehut.org">SourceHut</a>, which is now on-par with our git offering.
Special thanks are owed to SourceHut contributor Ludovic Chabant, who has been
instrumental in adding Mercurial support to SourceHut. You may have heard about
it while this was still experimental - but I&rsquo;m happy to tell you that we have
now completely integrated Mercurial support into SourceHut! Want to try it out?
Check out <a href="https://man.sr.ht/tutorials/set-up-account-and-hg.md">the tutorial</a>.</p>
<p>Mercurial support on SourceHut includes all of the trimmings, including CI
support via <a href="https://builds.sr.ht">builds.sr.ht</a> and email-driven collaboration
on <a href="https://lists.sr.ht">lists.sr.ht</a>. Of course, it&rsquo;s also 100%
free-as-in-freedom, open source software (<a href="https://hg.sr.ht/~sircmpwn/hg.sr.ht">hosted on
itself</a>) that you can <a href="https://man.sr.ht/hg.sr.ht/installation.md">deploy on your own
servers</a>. We&rsquo;ve tested hg.sr.ht
on some of the largest Mercurial repositories out there, including
mozilla-central and NetBSD src. The NetBSD project in particular has been very
helpful, walking us through their CVS to Hg conversion and stress-testing
hg.sr.ht with the resulting giant repositories. I&rsquo;m looking forward to working
more with them in the future!</p>
<p>The Mercurial community is actively innovating their software, and we&rsquo;ll be
right behind them. I&rsquo;m excited to provide a platform for elevating the Mercurial
community. There weren&rsquo;t a lot of good options for Mercurial fans before
SourceHut. Let&rsquo;s fix that together! SourceHut will be taking a more active role
in the Hg community, just like we have for git, and together we&rsquo;ll build a great
platform for software development.</p>
<p>I&rsquo;ll see you in Paris in May, at the <a href="https://www.mercurial-scm.org/pipermail/mercurial/2019-April/051196.html">inaugural Mercurial
conference</a>!</p>
<hr>
<p>Hg support on SourceHut was largely written by members of the Mercurial
community. If there are other version control communities interested in
SourceHut support, please <a href="mailto:~sircmpwn/sr.ht-dev@lists.sr.ht">reach out</a>!</p>
