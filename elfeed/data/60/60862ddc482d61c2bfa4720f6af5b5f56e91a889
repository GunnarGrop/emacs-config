<p>In January 2018, I wrote a blog post which included a <a href="https://drewdevault.com/2018/01/16/Fees-on-donation-platforms.html">fee
calculator</a>.
Patreon <a href="https://www.patreon.com/new-creator-plans">changes their fee model
tomorrow</a>, and it&rsquo;s time for an
updated calculator. I&rsquo;m grandfathered into the old fees, so not much has changed
for me, but I want to equip Patreon users - creators and supporters - with more
knowledge of how their money is moving through the platform.</p>
<p>Patreon makes money by siphoning some money off the top of a donation flow
between supporters and creators. Because of the nature of its business (a
private, VC-backed corporation), the siphon&rsquo;s size and semantics are prone to
change in undesirable ways, since VC&rsquo;s expect infinite growth and a private
business generally puts profit first. For this reason, I diversify my income, so
that when Patreon makes these changes it limits their impact on my financial
well-being.  Even so, Patreon is the biggest of my donation platforms,
representing over $500/month at the time of writing (<a href="https://drewdevault.com/donate/">full breakdown
here</a>)<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup>.</p>
<p>So, for any patrons who are curious about where their money goes, here&rsquo;s a handy
calculator to help you navigate the complex fees. Enjoy!</p>
<p><strong>Note</strong>: I don&rsquo;t normally ask you to share my posts, but the Patreon community
is too distributed for me to effectively reach them alone. Please share this
with your Patreon creators and communities!</p>
<p><noscript>Sorry, the calculator requires JavaScript.</noscript></p>
<div id="react-root"></div>
<script src="https://drewdevault.com/js/donation-calc.js"></script>
<p><strong>Note</strong>: this calculator does not include the withdrawal fee. When the creator
withdraws their funds from the platform, an additional fee is charged, but the
nature of that fee changes depending on the frequency with which they make
withdrawals and the total amount of money they make from all patrons - which is
information that&rsquo;s not easily available to the average patron for using with
this calculator. For details on the withdrawal fees, see <a href="https://support.patreon.com/hc/en-us/articles/203913489-What-are-my-options-to-receive-payout-">Patreon&rsquo;s support
article on the
subject</a>.</p>
<p>One question that&rsquo;s been left unanswered is how many times Patreon is going to
charge patrons for each creator they support. Previously, they batched payments
and only accordingly charged the payment processing fees once. However, along
with these changes, they&rsquo;re going to charge payment processing fees for each
creator, but they haven&rsquo;t lowered the payment processing fees. When we take a
look at our bank returns in the coming months, if Patreon is still batching
payments internally&hellip; hmm, where is the extra money going? We&rsquo;ll have to wait
and see.</p>
<h2 id="founding-creators">What are founding creators?</h2>
<p>Creators who used the Patreon platform prior to 2019-05-07 are &ldquo;founding
creators&rdquo;, and have different rates. They have different rates for each plan,
and lower payment processing fees. Founding creators are also not usually lite
creators, but were grandfathered into the pro plan.</p>
<h2 id="charge-up-front">What does charge up front mean?</h2>
<p>Some creators have the option to charge you as soon as you join the platform,
rather than once monthly or per-creation. This results in higher payment
processing fees for founding creators, as Patreon cannot batch the charge
alongside with your other creators.</p>
<h2 id="which-plan">How do I know what plan my creator uses?</h2>
<p>We can guess which plan our creator uses by looking at the features they use on
Patreon. Here are some giveaways:</p>
<ul>
<li>If they have different membership tiers, they use the Pro plan or better.</li>
<li>If they offer merch through Patreon, they use the Premium plan.</li>
</ul>
<p>You can also just reach out to your creator and ask!</p>
<!-- Hack to get footnotes from javascript to work -->
<p><span style="display: none"><sup id="fnref:2"><a href="#fn:2" class="footnote-ref" role="doc-noteref">2</a></sup></span></p>
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p>This is supplemented with my Sourcehut income as well, which is covered in the recent <a href="https://lists.sr.ht/~sircmpwn/sr.ht-discuss/%3C20190426160729.GC1351@homura.localdomain%3E">Q1 financial report</a>, as well as some <a href="https://drewdevault.com/consulting">consulting work</a>, which I don&rsquo;t publish numbers for. <a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:2" role="doc-endnote">
<p>This is an assumption based on public PayPal and Stripe payment processing rates. In practice, it&rsquo;s likely that Patreon has a volume discount with their payment processors. Patreon does not publish these rates. <a href="#fnref:2" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</section>
