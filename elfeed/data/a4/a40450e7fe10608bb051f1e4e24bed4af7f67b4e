<p>1,315 days after I started the <a href="https://swaywm.org">sway</a> project, it&rsquo;s finally
time for <a href="https://github.com/swaywm/sway/releases/tag/1.0">sway 1.0</a>! I had no
idea at the time how much work I was in for, or how many talented people would
join and support the project with me. In order to complete this project, we have
had to rewrite the entire Linux desktop nearly from scratch. Nearly 300 people
worked together, together writing over 9,000 commits and almost 100,000 lines of
code, to bring you this release.</p>
<p><small class="text-muted">Sway is an i3-compatible Wayland desktop for Linux and FreeBSD</small></p>
<p>1.0 is the first stable release of sway and represents a consistent, flexible,
and powerful desktop environment for Linux and FreeBSD. We hope you&rsquo;ll enjoy it!
If the last sway release you used was 0.15 or earlier, you&rsquo;re in for a shock.
0.15 was a buggy, frustrating desktop to use, but sway 1.0 has been completely
overhauled and represents a much more capable desktop. It&rsquo;s almost impossible to
summarize all of the changes which makes 1.0 great. Sway 1.0 adds a huge variety
of features which were sorely missed on 0.x, improves performance in every
respect, offers a more faithful implementation of Wayland, and exists as a
positive political force in the Wayland ecosystem pushing for standardization
and cooperation among Wayland projects.</p>
<p>When planning the future of sway, we realized that the Wayland ecosystem was
sorely in need of a stable &amp; flexible common base library to encapsulate all of
the difficult and complex facets of building a desktop. To this end, I decided
we would build <a href="https://github.com/swaywm/wlroots">wlroots</a>. It&rsquo;s been a
smashing success. This project has become very important to the Linux desktop
ecosystem, and the benefits we reap from it have been shared with the community
at large. <a href="https://github.com/swaywm/wlroots/wiki/Projects-which-use-wlroots">Dozens of projects</a> are using it today, and soon you&rsquo;ll
find it underneath most Linux desktops, on your phone, in your VR environment,
and more. Its influence extends beyond its borders as well, as we develop and
push for standards throughout Wayland.</p>
<p>Through this work we have also helped to build a broader ecosystem of tools
built on interoperable standards which you may find useful in your new sway 1.0
desktop. Here are a few of my favorites - each of which is compatible with many
Wayland compositors:</p>
<ul>
<li><a href="https://github.com/swaywm/swayidle">swayidle</a>: idle management daemon</li>
<li><a href="https://github.com/swaywm/swaylock">swaylock</a>: lock screen</li>
<li><a href="https://github.com/emersion/mako">mako</a>: notification daemon</li>
<li><a href="https://github.com/emersion/grim">grim</a>: screenshot tool</li>
<li><a href="https://github.com/emersion/slurp">slurp</a>: interactive region selection</li>
<li><a href="https://github.com/ammen99/wf-recorder">wf-recorder</a>: video capture tool</li>
<li><a href="https://github.com/Alexays/Waybar">waybar</a>: alternative panel</li>
<li><a href="https://source.puri.sm/Librem5/virtboard">virtboard</a>: on-screen keyboard</li>
<li><a href="https://github.com/bugaevc/wl-clipboard">wl-clipboard</a>: xclip replacement</li>
<li><a href="https://github.com/xyproto/wallutils">wallutils</a>: fancy wallpaper manager</li>
</ul>
<hr>
<p>None of this would be possible without the support of sway&rsquo;s and wlroots'
talented contributors. Hundreds of people worked together on this. I&rsquo;d like to
give special thanks to our core contributors: Brian Ashworth, Ian Fan, Ryan
Dwyer, Scott Anderson, and Simon Ser. Thanks are also in order for those who
have helped wlroots fit into the broader ecosystem - thanks to Purism for their
help on wlroots, KDE &amp; Canonical for their help on protocol standardization. I
also owe thanks to all of the other projects which use wlroots, particularly
including Way Cooler, Wayfire, and Waymonad, who all have made substantial
contributions to wlroots in their pursit of the best Wayland desktop.</p>
<p>I&rsquo;d also of course like to thank all of the users who have donated to support
my work, which I now do full-time, which has had and I hope will continue to
have a positive impact on the project and those around it. Please consider
<a href="https://drewdevault.com/donate">donating</a> to support the future of sway &amp;
wlroots if you haven&rsquo;t yet.</p>
<p>Though sway today is already stable and powerful, we&rsquo;re not done yet. We plan to
continue improving performance &amp; stability, adding useful desktop features,
taking advantage of better hardware, and bringing sway to more users. Here&rsquo;s
some of what we have planned for future releases:</p>
<ul>
<li>Better Wayland-native tools for internationalized input methods like CJK</li>
<li>Better accessibility tools including improved screen reader support,
high-contrast mode, a magnifying glass tool, and so on</li>
<li>Integration with xdg-portal &amp; pipewire for interoperable screen capture</li>
<li>Improved touch screen support for use on the <a href="https://puri.sm/products/librem-5/">Librem
5</a> and on
<a href="https://postmarketos.org/">postmarketOS</a></li>
<li>Better support for drawing tablets and additional hardware</li>
<li>Sandboxing and security features</li>
</ul>
<p>As with all sway features, we intend to have the best-in-class implementations
of these features and set the bar as high as we can for everyone else. We&rsquo;re
looking forward to your continued support!</p>
