<p>I just finished <a href="https://www.ifixit.com/Guide/s5/27077">replacing the micro-USB
daughterboard</a> on my Samsung Galaxy S5,
which involved taking the phone most of the way apart, doing the replacement,
and putting it back together. This inspired me to write about my approach to
maintaining my cell phone. I&rsquo;ve had this phone for a while and I have no plans
to upgrade - I backed the upcoming Purism phone, but I expect to spend
months/years on the software before I&rsquo;ll be using that as my daily driver.</p>
<p>I don&rsquo;t want to be buying a new phone every year. That&rsquo;s a lot of money! Though
the technophile in me finds the latest and greatest technology appealing, the
thought of doing my own repairs and upkeep on a battle-tested phone is equally
interesting. Here are the four things I&rsquo;ve found most important in phone upkeep.</p>
<h3 id="install-lineageos-or-replicant">Install LineageOS or Replicant</h3>
<p>Before I installed CyanogenMod when I bought this phone, I did some prying into
the stock ROM to see just how bad it was. It was even worse than I expected!
There were literally hundreds of apps and services with scary permissions
running in the background that could not be removed. These spy on you, wear down
your battery, and slow down your phone over time - another form of planned
obsolescence.</p>
<p>My phone is still as fast as the day I got it. It does a great job with
everything I ask it to do. The first thing you should do with every new phone is
install a third-party ROM - ideally, without Google apps. Stock ROMs suck, get
rid of it.</p>
<h3 id="insist-on-a-user-replacable-battery">Insist on a user-replacable battery</h3>
<p>Non-user-replacable batteries are an obvious form of planned obsolescence.
Batteries don&rsquo;t last forever and you should <em>never</em> buy a phone that you
cannot replace the battery of. A new battery for my S5 costs 10 bucks. 4 years
in, I&rsquo;ve replaced mine once and I can hold a charge fine for a couple of days.</p>
<h3 id="get-a-case">Get a case</h3>
<p>This one is pretty obvious, but I didn&rsquo;t follow this advice at first. I&rsquo;ve never
broken a screen, so I didn&rsquo;t bother with a case. When I decided I was going to
keep this phone for a long time, I went ahead and bought one. It doubles the
thickness of my phone but at least I can be sure I&rsquo;m not going to bust it up
when I drop it. It still fits in my pocket comfortably so it&rsquo;s no big deal.</p>
<h3 id="attempt-repairs-before-you-buy-a-new-phone">Attempt repairs before you buy a new phone</h3>
<p>The past couple of months, my phone&rsquo;s micro-USB3 port started to act up a bit. I
would have to wiggle the cable a bit to get it to take, and it could stop
charging if I rustled my desk the wrong way. I got a replacement USB
daughterboard on Amazon for 6 bucks. Replacing it took an hour, but when
removing the screen I broke the connection between my home button and my
motherboard - which was only 10 bucks for the replacement, including same day
shipping. The whole process was a lot easier than I thought it would be.</p>
<hr>
<p>Be a smart consumer when you&rsquo;re buying a phone. Insist on the replacable battery
and maybe read the iFixit teardown. Take good care of it and it&rsquo;ll last a long
time. Don&rsquo;t let consumerism get the better of you!</p>
