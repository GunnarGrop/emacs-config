<p>Recently I was making sure my main laptop is ready for travel<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup>, which mostly
just entails syncing up the latest version of my music collection. This laptop
is a Thinkpad X200, which turns 11 years old in July and is my main workstation
away from home (though I bring a second monitor and an external keyboard for
long trips). This laptop is a great piece of hardware. 100% of the hardware is
supported by the upstream Linux kernel, including the usual offenders like WiFi
and Bluetooth. Niche operating systems like 9front and Minix work great, too.
Even coreboot works! It&rsquo;s durable, user-serviceable, light, and still looks
brand new after all of these years. I love all of these things, but there&rsquo;s no
denying that it&rsquo;s 11 years behind on performance innovations.</p>
<p>Last year <a href="https://kde.org">KDE</a> generously <a href="https://drewdevault.com/2018/04/28/KDE-Sprint-retrospective.html">invited me</a> to and
sponsored my travel to their development sprint in Berlin. One of my friends
there teased me - in a friendly way - about my laptop, asking why I used such an
old system. There was a pensive moment when I answered: &ldquo;it forces me to
empathise with users who can&rsquo;t use high-end hardware&rdquo;. I showed him how it could
cold boot to a productive <a href="https://swaywm.org">sway</a> desktop in &lt;30 seconds,
then I installed KDE to compare. It doubled the amount of disk space in use,
took almost 10x as long to reach a usable desktop, and had severe rendering
issues with my old Intel GPU.</p>
<p>To be clear, KDE is a wonderful piece of software and my first recommendation to
most non-technical computer users who ask me for advice on using Linux. But
software often grows to use the hardware you give it. Software developers tend
to be computer enthusiasts, and use enthusiast-grade hardware. In reality, this
high-end hardware isn&rsquo;t really <em>necessary</em> for most applications outside of
video encoding, machine learning, and a few other domains.</p>
<p>I do have a more powerful workstation at home, but it&rsquo;s not really anything
special. I upgrade it very infrequently. I bought a new mid-range GPU which is
able to drive my four displays<sup id="fnref:2"><a href="#fn:2" class="footnote-ref" role="doc-noteref">2</a></sup> last year, I&rsquo;ve added the occasional hard
drive as it gets full, and I replaced the case with something lighter weight 3
years ago. Outside of those minor upgrades, I&rsquo;ve been using the same desktop
workstation for 7 years, and intend to use it for much longer. My servers are
similarly running on older hardware which is spec&rsquo;d to their needs (actually, I
left a lot of room to grow and <em>still</em> was able to buy old hardware).</p>
<p>My 11-year-old laptop can compile the Linux kernel from scratch in 20 minutes,
and it can play 1080p video in real-time. That&rsquo;s all I need! Many users cannot
afford high-end computer hardware, and most have better things to spend their
money on. And you know, I work hard for my money, too - if I can get a computer
which can do nearly 5 <em>billion</em> operations per second for $60, that should be
sufficient to solve nearly any problem. No doubt, there are faster laptops out
there, many of them with similarly impressive levels of compatibility with my
ideals. But why bother?</p>
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p>To <a href="https://fosdem.org/2019/">FOSDEM</a> - see you there! <a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:2" role="doc-endnote">
<p>I have a variety of displays and display configurations for the purpose of continuously testing sway/wlroots in those situations <a href="#fnref:2" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</section>
