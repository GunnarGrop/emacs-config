<p>I wrote sway&rsquo;s <a href="https://github.com/SirCmpwn/sway/commit/6a33e1e3cddac31b762e4376e29c03ccf8f92107">initial commit</a>
4 months ago, on August 4th. At the time of writing, there are now 1,070 commits
from 29 different authors, totalling 10,682 lines of C (and 1,176 lines of
header files). This has been done over the course of 256 pull requests and 118
issues. Of the 73 <a href="https://github.com/SirCmpwn/sway/issues/2">i3 features we&rsquo;re
tracking</a>, 51 are now supported, and
I&rsquo;ve been using sway as my daily driver for a while now. Today, sway looks like
this:</p>
<p><a href="https://sr.ht/NCx_.png"><img src="https://sr.ht/NCx_.png" alt=""></a></p>
<p>For those who are new to the project, <a href="https://github.com/SirCmpwn/sway">sway</a>
is an i3-compatible Wayland compositor. That is, your existing
<a href="http://i3wm.org/">i3</a> configuration file will work as-is on sway, and your
keybindings will be the same and the colors and font configuration will be the
same, and so on. It&rsquo;s i3, but on Wayland.</p>
<p>Sway initially made the rounds on <a href="https://redd.it/3he5hn">/r/linux</a> and
<a href="https://redd.it/3he48j">/r/i3wm</a> and
<a href="https://www.phoronix.com/scan.php?page=news_item&amp;px=Wayland-i3-Sway-Tiling">Phoronix</a>
on August 17th, 13 days after the initial commit. I was already dogfooding it by
then, but now I&rsquo;m actually using it 100% of the time, and I hear others have
started to as well. What&rsquo;s happened since then? Well:</p>
<ul>
<li>Floating windows</li>
<li>Multihead support</li>
<li>XDG compliant config</li>
<li>Fullscreen windows</li>
<li>gaps</li>
<li>IPC</li>
<li>Window criteria</li>
<li>58 i3 commands and 1 command unique to sway</li>
<li>Wallpaper support</li>
<li>Resizing/moving tiled windows with the mouse</li>
<li>swaymsg, swaylock, <strong>swaybar</strong> as in i3-msg, i3lock, i3bar</li>
<li>Hundreds of bug fixes and small improvements</li>
</ul>
<p>Work on sway has also driven improvements in our dependencies, such as
<a href="https://github.com/Cloudef/wlc">wlc</a>, which now has improved xwayland support,
support for Wayland protocol extensions (which makes swaybg and swaylock and
swaybar possible), and various bugfixes and small features added at the bequest
of sway. Special thanks to Cloudef for helping us out with so many things!</p>
<p>All of this is only possible thanks to the hard work of dozens of contributors.
Here&rsquo;s the breakdown of <strong>lines of code per author</strong> for the top ten authors:</p>
<table class="table">
    <tbody>
        <tr><td>3516</td><td>Drew DeVault</td></tr>
        <tr><td>2400</td><td>taiyu</td></tr>
        <tr><td>1786</td><td>S. Christoffer Eliesen</td></tr>
        <tr><td>1127</td><td>Mikkel Oscar Lyderik</td></tr>
        <tr><td>720</td><td>Luminarys</td></tr>
        <tr><td>534</td><td>minus</td></tr>
        <tr><td>200</td><td>Christoph Gysin</td></tr>
        <tr><td>121</td><td>Yacine Hmito</td></tr>
        <tr><td>79</td><td>Kevin Hamacher</td></tr>
    </tbody>
</table>
<p>And here&rsquo;s the total <strong>number of commits per author</strong> for each of the top 10
committers:</p>
<table class="table">
    <tbody>
        <tr><td>514</td><td> Drew DeVault</td></tr>
        <tr><td>191</td><td> taiyu</td></tr>
        <tr><td>102</td><td> S. Christoffer Eliesen</td></tr>
        <tr><td>97</td><td> Luminarys</td></tr>
        <tr><td>56</td><td> Mikkel Oscar Lyderik</td></tr>
        <tr><td>46</td><td> Christoph Gysin</td></tr>
        <tr><td>34</td><td> minus</td></tr>
        <tr><td>9</td><td> Ben Boeckel</td></tr>
        <tr><td>6</td><td> Half-Shot</td></tr>
        <tr><td>6</td><td> jdiez17</td></tr>
    </tbody>
</table>
<p>As the maintainer of sway, <em>a lot</em> of what I do is reviewing and merging
contributions from others. So these statistics change a bit if we use <strong>number
of commits per author, excluding merge commits</strong>:</p>
<table class="table">
    <tbody>
        <tr><td>279</td><td> Drew DeVault</td></tr>
        <tr><td>175</td><td> taiyu</td></tr>
        <tr><td>102</td><td> S. Christoffer Eliesen</td></tr>
        <tr><td>96</td><td> Luminarys</td></tr>
        <tr><td>56</td><td> Mikkel Oscar Lyderik</td></tr>
        <tr><td>46</td><td> Christoph Gysin</td></tr>
        <tr><td>34</td><td> minus</td></tr>
        <tr><td>9</td><td> Ben Boeckel</td></tr>
        <tr><td>6</td><td> jdiez17</td></tr>
        <tr><td>5</td><td> Yacine Hmito</td></tr>
    </tbody>
</table>
<p>These stats only cover the top ten in each, but there are more - check out the
<a href="https://github.com/SirCmpwn/sway/graphs/contributors">full list</a>.</p>
<p>So, what does this all mean for sway? Well, it&rsquo;s going very well. If you&rsquo;d like
to live on the edge, you can use sway right now and have a productive workflow.
The important features that are missing include stacking and tabbed layouts,
window borders, and some features on the bar. I&rsquo;m looking at starting up a beta
when these features are finished. Come try out sway! Test it with us, open
GitHub issues with your gripes and desires, and <a href="http://webchat.freenode.net/?channels=sway&amp;uio=d4">chat
with us on IRC</a>.</p>
<p><em>This blog post was composed from sway.</em></p>
