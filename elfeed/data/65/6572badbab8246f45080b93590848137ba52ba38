<p>I have never been angrier about the corporate surveillance complex, which I have
rallied against for <em>years</em>, than I am today. Buying and selling user&rsquo;s private
information on the open market is bad enough for the obvious reasons, but today,
I learned that the depths of depravity this market will descend to are without
limit. Today I am more angry and ashamed at this industry than I have ever been.
Corporate surveillance and adtech has turned your phone into an informant
against you and brought about the actual <strong>murder</strong> of the user.</p>
<p><a href="https://www.vice.com/en/article/y3g97x/location-data-apps-drone-strikes-iowa-national-guard">Vice: Military Unit That Conducts Drone Strikes Bought Location Data From Ordinary Apps</a></p>
<p>Say you&rsquo;re a Muslim. You download some apps for reading the Quran and
participating in Muslim-oriented social networks. These ads steal whatever
personal information it can get its hands on, through any means available, and
sell it to <a href="https://trademarks.justia.com/874/53/locate-87453515.html">Locate X</a>, who stores every GPS location your phone has visited
and tags it as being associated with a Muslim. This is used, say, to place
Muslim-targeted ads on billboards in Muslim-dense areas. It&rsquo;s also sold to the
Iowa National Guard, who uses it to conduct drone strikes. The app you installed
is selling your GPS data so it can be used to <em>kill you</em>.</p>
<p>For a long time, I have preached &ldquo;respect the user&rdquo;. I want us, as programmers,
to treat the user with the same standards of common decency and respect we&rsquo;d
afford to our neighbors. It seems I have to revise my sermon to &ldquo;don&rsquo;t murder
the user&rdquo;! If you work at a company which surveilles its users, <a href="https://drewdevault.com/2020/05/05/We-are-complicit-in-our-employers-deeds.html">you are
complicit</a> in these murders. You have written software which is used to
<em>murder people</em>.</p>
<p>This industry is in severe need of a moral health check. You, the reader of
this article, need to take personal responsibility for what your code is doing.
Your boss isn&rsquo;t going to. Do you really know what that database is being used
for, or who it&rsquo;s being sold to, or who it <em>might</em> be sold to in the future? Most
companies include their hoard of private, personal information about their users
as part of their valuation. Do you have stock options, by the way?</p>
<p>I&rsquo;ve often heard the excuse that employees of large surveillance companies &ldquo;want
to feed their families, like anyone else&rdquo;. Well, thanks to your work, a child
you&rsquo;ve never met was orphaned, and doesn&rsquo;t have a family anymore. Who&rsquo;s going to
feed them? Is there really no other way for you to support your family?</p>
<p>Don&rsquo;t fucking kill your users.</p>
