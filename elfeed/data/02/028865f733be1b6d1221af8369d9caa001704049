<p>Greetings! Today is another rainy day here in Philadelphia, which rather sours
my plans of walking over to the nearby cafe to order some breakfast to-go. But I
am tired, and if I&rsquo;m going to make it to the end of this blog post in one piece,
I&rsquo;m gonna need a coffee. brb.</p>
<p>Hey, that was actually pretty refreshing. It&rsquo;s just drizzling, and the rain is
nice and cool. Alright, here goes! What&rsquo;s new? I&rsquo;ll leave the Wayland news for
<a href="https://emersion.fr/blog">Simon Ser&rsquo;s blog</a> this month - he&rsquo;s been working on
some exciting stuff. The <a href="https://baremessages.org/">BARE encoding</a> announced
last month has received some great feedback and refinements, and there are now
six projects providing BARE support for their author&rsquo;s favorite programming
language<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup>. There have also been some improvements to the Go implementation
which should help with some SourceHut plans later on.</p>
<p>On the subject of SourceHut, I&rsquo;ve focused mainly on infrastructure improvements
this month. There is a new server installed for hg.sr.ht, which will also be
useful as a testbed for additional ops work planned for future expansion.
Additionally, the PostgreSQL backup system has been overhauled and made more
resilient, both to data loss and to outages. A lot of other robustness
improvements have been made fleet-wide in monitoring. I&rsquo;ll be working on more
user-facing features again next month, but in the meanwhile, contributors like
наб have sent many patches in which I&rsquo;ll cover in detail in the coming &ldquo;What&rsquo;s
cooking&rdquo; post for <a href="https://sourcehut.org/blog">sourcehut.org</a>.</p>
<p>Otherwise, I&rsquo;ve been taking it easy this month. I definitely haven&rsquo;t been
spending a lot of my time on a secret project, no sir. Thanks again for your
support! I&rsquo;ll see you next month.</p>
<details>
<summary>?</summary>
<pre>
use io;
use io_uring = linux::io_uring;
use linux;
use strings;
<p>export fn main void = {
let uring = match (io_uring::init(256u32, 0u32)) {
err: linux::error =&gt; {
io::println(&ldquo;io_uring::init error:&quot;);
io::println(linux::errstr(err));
return;
},
u: io_uring::io_uring =&gt; u,
};</p>
<pre><code>let buf: [8192]u8 = [0u8...];
let text: nullable *str = null;
let wait = 0u;
let offs = 0z;
let read: *io_uring::sqe = null: *io_uring::sqe,
    write: *io_uring::sqe = null: *io_uring::sqe;
let eof = false;

while (!eof) {
    read = io_uring::must_get_sqe(&amp;uring);
    io_uring::prep_read(read, linux::STDIN_FILENO,
        &amp;buf, len(buf): u32, offs);
    io_uring::sqe_set_user_data(read, &amp;read);
    wait += 1u;

    let ev = match (io_uring::submit_and_wait(&amp;uring, wait)) {
        err: linux::error =&gt; {
            io::println(&quot;io_uring::submit error:&quot;);
            io::println(linux::errstr(err));
            return;
        },
        ev: uint =&gt; ev,
    };

    wait -= ev;

    for (let i = 0; i &lt; ev; i += 1) {
        let cqe = match (io_uring::get_cqe(&amp;uring, 0u, 0u)) {
            err: linux::error =&gt; {
                io::println(&quot;io_uring::get_cqe error:&quot;);
                io::println(linux::errstr(err));
                return;
            },
            c: *io_uring::cqe =&gt; c,
        };

        if (io_uring::cqe_get_user_data(cqe) == &amp;read) {
            if (text != null) {
                free(text);
            };

            if (cqe.res == 0) {
                eof = true;
                break;
            };

            text = strings::must_decode_utf8(buf[0..cqe.res]);
            io_uring::cqe_seen(&amp;uring, cqe);

            write = io_uring::must_get_sqe(&amp;uring);
            io_uring::prep_write(write, linux::STDOUT_FILENO,
                text: *char, len(text): u32, 0);
            io_uring::sqe_set_user_data(write, &amp;write);
            wait += 1u;
            offs += cqe.res;
        } else if (io_uring::cqe_get_user_data(cqe) == &amp;write) {
            assert(cqe.res &gt; 0);
            io_uring::cqe_seen(&amp;uring, cqe);
        } else {
            assert(false, &quot;Unknown CQE user data&quot;);
        };
    };
};

io_uring::close(&amp;uring);
</code></pre>
<p>};
</pre></p>
<details>
<summary>hmm?</summary>
<p>I might note that I wrote this program to test my io_uring wrapper; it's not
representative of how normal programs will do I/O in the future.</p>
</details>
</details>
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p>Or in some cases, the language the author is begrudgingly stuck with. <a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</section>
