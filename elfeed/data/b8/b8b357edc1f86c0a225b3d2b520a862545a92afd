<p>The other day, I saw <a href="https://github.com/zeeshanu/learn-regex">Learn regex the easy
way</a>. This is a great resource, but I
felt the need to pen a post explaining that regexes are usually not the right
approach.</p>
<p>Let&rsquo;s do a little exercise. I googled &ldquo;URL regex&rdquo; and here&rsquo;s the first Stack
Overflow result:</p>
<pre><code>https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&amp;//=]*)
</code></pre><p style="text-align: right">
<small><a href="https://stackoverflow.com/a/3809435/1191610">source</a></small>
</p>
<p>This is a bad regex. Here are some valid URLs that this regex fails to match:</p>
<ul>
<li>http://x.org</li>
<li><a href="http://nic.science">http://nic.science</a></li>
<li>http://名がドメイン.com (warning: this is a parked domain)</li>
<li><a href="http://example.org/url,with,commas">http://example.org/url,with,commas</a></li>
<li><a href="https://en.wikipedia.org/wiki/Harry_Potter_(film_series)">https://en.wikipedia.org/wiki/Harry_Potter_(film_series)</a></li>
<li>http://127.0.0.1</li>
<li>http://[::1] (ipv6 loopback)</li>
</ul>
<p>Here are some invalid URLs the regex is fine with:</p>
<ul>
<li><a href="http://exam..ple.org">http://exam..ple.org</a></li>
<li><a href="http://--example.org">http://--example.org</a></li>
</ul>
<p>This answer has been revised 9 times on Stack Overflow, and this is the best
they could come up with. Go back and read the regex. Can you tell where each of
these bugs are? How long did it take you? If you received a bug report in your
application because one of these URLs was handled incorrectly, do you understand
this regex well enough to fix it? If your application has a URL regex, go find
it and see how it fares with these tests.</p>
<p>Complicated regexes are opaque, unmaintainable, and often wrong. The correct
approach to validating a URL is as follows:</p>
<div class="highlight"><pre class="chroma"><code class="language-python" data-lang="python"><span class="kn">from</span> <span class="nn">urllib.parse</span> <span class="kn">import</span> <span class="n">urlparse</span>

<span class="k">def</span> <span class="nf">is_url_valid</span><span class="p">(</span><span class="n">url</span><span class="p">):</span>
    <span class="k">try</span><span class="p">:</span>
        <span class="n">urlparse</span><span class="p">(</span><span class="n">url</span><span class="p">)</span>
        <span class="k">return</span> <span class="bp">True</span>
    <span class="k">except</span><span class="p">:</span>
        <span class="k">return</span> <span class="bp">False</span>
</code></pre></div><p>A regex is useful for validating <em>simple</em> patterns and for <em>finding</em> patterns in
text. For anything beyond that it&rsquo;s almost certainly a terrible choice. Say you
want to&hellip;</p>
<p><strong>validate an email address</strong>: try to send an email to it!</p>
<p><strong>validate password strength requirements</strong>: estimate the complexity with
<a href="https://github.com/dropbox/zxcvbn">zxcvbn</a>!</p>
<p><strong>validate a date</strong>: use your standard library!
<a href="https://docs.python.org/3.6/library/datetime.html#datetime.datetime.strptime">datetime.datetime.strptime</a></p>
<p><strong>validate a credit card number</strong>: run the <a href="https://en.wikipedia.org/wiki/Luhn_algorithm">Luhn
algorithm</a> on it!</p>
<p><strong>validate a social security number</strong>: alright, use a regex. But don&rsquo;t expect
the number to be assigned to someone until you ask the Social Security
Administration about it!</p>
<p>Get the picture?</p>
