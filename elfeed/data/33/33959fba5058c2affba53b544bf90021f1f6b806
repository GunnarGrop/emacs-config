<p><em>Note: this is a redacted copy of a blog post published on the internal
development blog of a new systems programming language. The name of the project
and further details are deliberately being kept in confidence until the initial
release. You may be able to find it if you look hard enough — you have my
thanks in advance for keeping it to yourself. For more information, see &ldquo;<a href="https://drewdevault.com/2021/03/19/A-new-systems-language.html">We are
building a new systems programming language</a>&rdquo;.</em></p>
<style>
.redacted {
  background: black;
  foreground: black;
}
</style>
<p>I&rsquo;ve just merged support for reflection in <span class="redacted">xxxx</span>.
Here&rsquo;s how it works!</p>
<h2 id="background">Background</h2>
<p>&ldquo;Reflection&rdquo; refers to the ability for a program to examine the type system of
its programming language, and to dynamically manipulate types and their values
at runtime. You can learn more at <a href="https://en.wikipedia.org/wiki/Reflective_programming">Wikipedia</a>.</p>
<h2 id="reflection-from-a-user-perspective">Reflection from a user perspective</h2>
<p>Let&rsquo;s start with a small sample program:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="cp">use fmt;</span>
<span class="cp">use types;</span>

<span class="k">export</span> <span class="k">fn</span> <span class="n">main</span><span class="p">()</span> <span class="kt">void</span> <span class="o">=</span> <span class="p">{</span>
    <span class="k">const</span> <span class="n">my_type</span><span class="o">:</span> <span class="k">type</span> <span class="o">=</span> <span class="k">type</span><span class="p">(</span><span class="kt">int</span><span class="p">);</span>
    <span class="k">const</span> <span class="n">typeinfo</span><span class="o">:</span> <span class="o">*</span><span class="n">types</span><span class="o">::</span><span class="n">typeinfo</span> <span class="o">=</span> <span class="n">types</span><span class="o">::</span><span class="n">reflect</span><span class="p">(</span><span class="n">my_type</span><span class="p">);</span>
    <span class="n">fmt</span><span class="o">::</span><span class="n">printfln</span><span class="p">(</span><span class="s">&#34;int</span><span class="se">\n</span><span class="s">id: {}</span><span class="se">\n</span><span class="s">size: {}</span><span class="se">\n</span><span class="s">alignment: {}&#34;</span><span class="p">,</span>
        <span class="n">typeinfo</span><span class="p">.</span><span class="n">id</span><span class="p">,</span> <span class="n">typeinfo</span><span class="p">.</span><span class="n">sz</span><span class="p">,</span> <span class="n">typeinfo</span><span class="p">.</span><span class="n">al</span><span class="p">)</span><span class="o">!</span><span class="p">;</span>
<span class="p">};</span>
</code></pre></div><p>Running this program produces the following output:</p>
<pre><code>int
id: 1099590421
size: 4
alignment: 4
</code></pre><p>This gives us a simple starting point to look at. We can see that &ldquo;type&rdquo; is used
as the type of the &ldquo;my_type&rdquo; variable, and initialized with a &ldquo;type(int)&rdquo;
expression. This expression returns a type value for the type given in the
parenthesis — in this case, for the &ldquo;int&rdquo; type.</p>
<p>To learn anything useful, we have to convert this to a &ldquo;types::typeinfo&rdquo;
pointer, which we do via <code>types::reflect</code>. The typeinfo structure looks like
this:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="k">type</span> <span class="n">typeinfo</span> <span class="o">=</span> <span class="k">struct</span> <span class="p">{</span>
	<span class="n">id</span><span class="o">:</span> <span class="kt">uint</span><span class="p">,</span>
	<span class="n">sz</span><span class="o">:</span> <span class="k">size</span><span class="p">,</span>
	<span class="n">al</span><span class="o">:</span> <span class="k">size</span><span class="p">,</span>
	<span class="n">flags</span><span class="o">:</span> <span class="n">flags</span><span class="p">,</span>
	<span class="n">repr</span><span class="o">:</span> <span class="n">repr</span><span class="p">,</span>
<span class="p">};</span>
</code></pre></div><p>The ID field is the type&rsquo;s unique identifier, which is universally unique and
deterministic, and forms part of <span class="redacted">xxxx</span>&rsquo;s ABI. This
is derived from an FNV-32 hash of the type information. You can find the ID for
any type by modifying our little example program, or you can use the helper
program in the <code>cmd/<span class="redacted">xxxx</span>type</code> directory
of the <span class="redacted">xxxx</span> source tree.</p>
<p>Another important field is the &ldquo;repr&rdquo; field, which is short for
&ldquo;representation&rdquo;, and it gives details about the inner structure of the type.
The repr type is defined as a tagged union of all possible type representations
in the <span class="redacted">xxxx</span> type system:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="k">type</span> <span class="n">repr</span> <span class="o">=</span> <span class="p">(</span><span class="n">alias</span> <span class="o">|</span> <span class="n">array</span> <span class="o">|</span> <span class="n">builtin</span> <span class="o">|</span> <span class="n">enumerated</span> <span class="o">|</span> <span class="n">func</span> <span class="o">|</span> <span class="n">pointer</span> <span class="o">|</span> <span class="n">slice</span> <span class="o">|</span> <span class="n">struct_union</span> <span class="o">|</span> <span class="n">tagged</span> <span class="o">|</span> <span class="n">tuple</span><span class="p">);</span>
</code></pre></div><p>In the case of the &ldquo;int&rdquo; type, the representation is &ldquo;builtin&rdquo;:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="k">type</span> <span class="n">builtin</span> <span class="o">=</span> <span class="n">enum</span> <span class="kt">uint</span> <span class="p">{</span>
  <span class="n">BOOL</span><span class="p">,</span> <span class="n">CHAR</span><span class="p">,</span> <span class="n">F32</span><span class="p">,</span> <span class="n">F64</span><span class="p">,</span> <span class="n">I16</span><span class="p">,</span> <span class="n">I32</span><span class="p">,</span> <span class="n">I64</span><span class="p">,</span> <span class="n">I8</span><span class="p">,</span> <span class="n">INT</span><span class="p">,</span> <span class="n">NULL</span><span class="p">,</span> <span class="n">RUNE</span><span class="p">,</span> <span class="n">SIZE</span><span class="p">,</span> <span class="n">STR</span><span class="p">,</span> <span class="n">U16</span><span class="p">,</span> <span class="n">U32</span><span class="p">,</span>
  <span class="n">U64</span><span class="p">,</span> <span class="n">U8</span><span class="p">,</span> <span class="n">UINT</span><span class="p">,</span> <span class="n">UINTPTR</span><span class="p">,</span> <span class="n">VOID</span><span class="p">,</span> <span class="n">TYPE</span><span class="p">,</span>
<span class="p">};</span>
</code></pre></div><p><code>builtin::INT</code>, in this case. The structure and representation of the &ldquo;int&rdquo; type
is defined by the <span class="redacted">xxxx</span> specification and cannot be
overridden by the program, so no further information is necessary. The relevant
part of the spec is:</p>
<p><img src="https://l.sr.ht/Sbcb.png" alt="&ldquo;The precision of &lsquo;int&rsquo; and &lsquo;uint&rsquo; are implementation-defined. &lsquo;int&rsquo; shall be signed, and &lsquo;uint&rsquo; shall be unsigned. Both types shall be at least 32-bits in precision. The precision in bits shall be a power of two."">
<img src="https://l.sr.ht/oZw4.png" alt="A table from the specification showing the precision ranges of each integer type"></p>
<p>More information is provided for more complex types, such as structs.</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="cp">use fmt;</span>
<span class="cp">use types;</span>

<span class="k">export</span> <span class="k">fn</span> <span class="n">main</span><span class="p">()</span> <span class="kt">void</span> <span class="o">=</span> <span class="p">{</span>
	<span class="k">const</span> <span class="n">my_type</span><span class="o">:</span> <span class="k">type</span> <span class="o">=</span> <span class="k">type</span><span class="p">(</span><span class="k">struct</span> <span class="p">{</span>
		<span class="n">x</span><span class="o">:</span> <span class="kt">int</span><span class="p">,</span>
		<span class="n">y</span><span class="o">:</span> <span class="kt">int</span><span class="p">,</span>
	<span class="p">});</span>
	<span class="k">const</span> <span class="n">typeinfo</span><span class="o">:</span> <span class="o">*</span><span class="n">types</span><span class="o">::</span><span class="n">typeinfo</span> <span class="o">=</span> <span class="n">types</span><span class="o">::</span><span class="n">reflect</span><span class="p">(</span><span class="n">my_type</span><span class="p">);</span>
	<span class="n">fmt</span><span class="o">::</span><span class="n">printfln</span><span class="p">(</span><span class="s">&#34;id: {}</span><span class="se">\n</span><span class="s">size: {}</span><span class="se">\n</span><span class="s">alignment: {}&#34;</span><span class="p">,</span>
		<span class="n">typeinfo</span><span class="p">.</span><span class="n">id</span><span class="p">,</span> <span class="n">typeinfo</span><span class="p">.</span><span class="n">sz</span><span class="p">,</span> <span class="n">typeinfo</span><span class="p">.</span><span class="n">al</span><span class="p">)</span><span class="o">!</span><span class="p">;</span>
	<span class="k">const</span> <span class="n">st</span> <span class="o">=</span> <span class="n">typeinfo</span><span class="p">.</span><span class="n">repr</span> <span class="n">as</span> <span class="n">types</span><span class="o">::</span><span class="n">struct_union</span><span class="p">;</span>
	<span class="k">assert</span><span class="p">(</span><span class="n">st</span><span class="p">.</span><span class="n">kind</span> <span class="o">==</span> <span class="n">types</span><span class="o">::</span><span class="n">struct_kind</span><span class="o">::</span><span class="n">STRUCT</span><span class="p">);</span>
	<span class="k">for</span> <span class="p">(</span><span class="k">let</span> <span class="n">i</span> <span class="o">=</span> <span class="mi">0</span><span class="n">z</span><span class="p">;</span> <span class="n">i</span> <span class="o">&lt;</span> <span class="k">len</span><span class="p">(</span><span class="n">st</span><span class="p">.</span><span class="n">fields</span><span class="p">);</span> <span class="n">i</span> <span class="o">+=</span> <span class="mi">1</span><span class="p">)</span> <span class="p">{</span>
		<span class="k">const</span> <span class="n">field</span> <span class="o">=</span> <span class="n">st</span><span class="p">.</span><span class="n">fields</span><span class="p">[</span><span class="n">i</span><span class="p">];</span>
		<span class="k">assert</span><span class="p">(</span><span class="n">field</span><span class="p">.</span><span class="n">type_</span> <span class="o">==</span> <span class="k">type</span><span class="p">(</span><span class="kt">int</span><span class="p">));</span>
		<span class="n">fmt</span><span class="o">::</span><span class="n">printfln</span><span class="p">(</span><span class="s">&#34;</span><span class="se">\t</span><span class="s">{}: offset {}&#34;</span><span class="p">,</span> <span class="n">field</span><span class="p">.</span><span class="n">name</span><span class="p">,</span> <span class="n">field</span><span class="p">.</span><span class="n">offs</span><span class="p">)</span><span class="o">!</span><span class="p">;</span>
	<span class="p">};</span>
<span class="p">};</span>
</code></pre></div><p>The output of this program is:</p>
<pre><code>id: 2617358403
size: 8
alignment: 4
	x: offset 0
	y: offset 4
</code></pre><p>Here the &ldquo;repr&rdquo; field provides the &ldquo;types::struct_union&rdquo; structure:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="k">type</span> <span class="n">struct_union</span> <span class="o">=</span> <span class="k">struct</span> <span class="p">{</span>
	<span class="n">kind</span><span class="o">:</span> <span class="n">struct_kind</span><span class="p">,</span>
	<span class="n">fields</span><span class="o">:</span> <span class="p">[]</span><span class="n">struct_field</span><span class="p">,</span>
<span class="p">};</span>

<span class="k">type</span> <span class="n">struct_kind</span> <span class="o">=</span> <span class="n">enum</span> <span class="p">{</span>
	<span class="n">STRUCT</span><span class="p">,</span>
	<span class="n">UNION</span><span class="p">,</span>
<span class="p">};</span>

<span class="k">type</span> <span class="n">struct_field</span> <span class="o">=</span> <span class="k">struct</span> <span class="p">{</span>
	<span class="n">name</span><span class="o">:</span> <span class="kt">str</span><span class="p">,</span>
	<span class="n">offs</span><span class="o">:</span> <span class="k">size</span><span class="p">,</span>
	<span class="n">type_</span><span class="o">:</span> <span class="k">type</span><span class="p">,</span>
<span class="p">};</span>
</code></pre></div><p>Makes sense? Excellent. So how does it all work?</p>
<h2 id="reflection-internals">Reflection internals</h2>
<p>Let me first draw the curtain back from the magic &ldquo;types::reflect&rdquo; function:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="c1">// Returns [[typeinfo]] for the provided type.
</span><span class="c1"></span><span class="k">export</span> <span class="k">fn</span> <span class="n">reflect</span><span class="p">(</span><span class="n">in</span><span class="o">:</span> <span class="k">type</span><span class="p">)</span> <span class="k">const</span> <span class="o">*</span><span class="n">typeinfo</span> <span class="o">=</span> <span class="n">in</span><span class="o">:</span> <span class="o">*</span><span class="n">typeinfo</span><span class="p">;</span>
</code></pre></div><p>It simply casts the &ldquo;type&rdquo; value to a pointer, which is what it is. When the
compiler sees an expression like <code>let x = type(int)</code>, it statically allocates
the typeinfo data structure into the program and returns a pointer to it, which
is then wrapped up in the opaque &ldquo;type&rdquo; meta-type. The &ldquo;reflect&rdquo; function simply
converts it to a useful pointer. Here&rsquo;s the generated IR for this:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="o">%</span><span class="n">binding</span><span class="mf">.4</span> <span class="o">=</span><span class="n">l</span> <span class="n">alloc8</span> <span class="mi">8</span>
<span class="n">storel</span> <span class="err">$</span><span class="n">rt</span><span class="p">.</span><span class="n">builtin_int</span><span class="p">,</span> <span class="o">%</span><span class="n">binding</span><span class="mf">.4</span>
</code></pre></div><p>A clever eye will note that we initialize the value to a pointer to
&ldquo;rt.builtin_int&rdquo;, rather than allocating a typeinfo structure here and now. The
runtime module provides static typeinfos for all built-in types, which look like
this:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="k">export</span> <span class="k">const</span> <span class="cp">@hidden</span> <span class="n">builtin_int</span><span class="o">:</span> <span class="n">types</span><span class="o">::</span><span class="n">typeinfo</span> <span class="o">=</span> <span class="n">types</span><span class="o">::</span><span class="n">typeinfo</span> <span class="p">{</span>
	<span class="n">id</span> <span class="o">=</span> <span class="mi">1099590421</span><span class="p">,</span>
	<span class="n">sz</span> <span class="o">=</span> <span class="mi">4</span><span class="p">,</span> <span class="n">al</span> <span class="o">=</span> <span class="mi">4</span><span class="p">,</span> <span class="n">flags</span> <span class="o">=</span> <span class="mi">0</span><span class="p">,</span>
	<span class="n">repr</span> <span class="o">=</span> <span class="n">types</span><span class="o">::</span><span class="n">builtin</span><span class="o">::</span><span class="n">INT</span><span class="p">,</span>
<span class="p">};</span>
</code></pre></div><p>These are an internal implementation detail, hence &ldquo;@hidden&rdquo;. But many types are
not built-in, so the compiler is required to statically allocate a typeinfo
structure:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="k">export</span> <span class="k">fn</span> <span class="n">main</span><span class="p">()</span> <span class="kt">void</span> <span class="o">=</span> <span class="p">{</span>
	<span class="k">let</span> <span class="n">x</span> <span class="o">=</span> <span class="k">type</span><span class="p">(</span><span class="k">struct</span> <span class="p">{</span> <span class="n">x</span><span class="o">:</span> <span class="kt">int</span><span class="p">,</span> <span class="n">y</span><span class="o">:</span> <span class="kt">int</span> <span class="p">});</span>
<span class="p">};</span>
</code></pre></div><pre><code>data $strdata.7 = section &quot;.data.strdata.7&quot; { b &quot;x&quot; }

data $strdata.8 = section &quot;.data.strdata.8&quot; { b &quot;y&quot; }

data $sldata.6 = section &quot;.data.sldata.6&quot; {
  l $strdata.7, l 1, l 1, l 0, l $rt.builtin_int,
  l $strdata.8, l 1, l 1, l 4, l $rt.builtin_int,
}

data $typeinfo.5 = section &quot;.data.typeinfo.5&quot; {
  w 2617358403, z 4,
  l 8,
  l 4,
  w 0, z 4,
  w 5555256, z 4,
  w 0, z 4,
  l $sldata.6, l 2, l 2,
}

export function section &quot;.text.main&quot; &quot;ax&quot; $main() {
@start.0
	%binding.4 =l alloc8 8
@body.1
	storel $typeinfo.5, %binding.4
@.2
	ret
}
</code></pre><p>This has the unfortunate effect of re-generating all of these typeinfo
structures every time someone uses <code>type(struct { x: int, y: int })</code>. We still
have one trick up our sleeve, though: type aliases! Most people don&rsquo;t actually
use anonymous structs like this often, preferring to use a type alias to give
them a name like &ldquo;coords&rdquo;. When they do this, the situation improves:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="k">type</span> <span class="n">coords</span> <span class="o">=</span> <span class="k">struct</span> <span class="p">{</span> <span class="n">x</span><span class="o">:</span> <span class="kt">int</span><span class="p">,</span> <span class="n">y</span><span class="o">:</span> <span class="kt">int</span> <span class="p">};</span>

<span class="k">export</span> <span class="k">fn</span> <span class="n">main</span><span class="p">()</span> <span class="kt">void</span> <span class="o">=</span> <span class="p">{</span>
	<span class="k">let</span> <span class="n">x</span> <span class="o">=</span> <span class="k">type</span><span class="p">(</span><span class="n">coords</span><span class="p">);</span>
<span class="p">};</span>
</code></pre></div><pre><code>data $strdata.1 = section &quot;.data.strdata.1&quot; { b &quot;coords&quot; }

data $sldata.0 = section &quot;.data.sldata.0&quot; { l $strdata.1, l 6, l 6 }

data $strdata.4 = section &quot;.data.strdata.4&quot; { b &quot;x&quot; }

data $strdata.5 = section &quot;.data.strdata.5&quot; { b &quot;y&quot; }

data $sldata.3 = section &quot;.data.sldata.3&quot; {
  l $strdata.4, l 1, l 1, l 0, l $rt.builtin_int,
  l $strdata.5, l 1, l 1, l 4, l $rt.builtin_int,
}

data $typeinfo.2 = section &quot;.data.typeinfo.2&quot; {
  w 2617358403, z 4,
  l 8,
  l 4,
  w 0, z 4,
  w 5555256, z 4,
  w 0, z 4,
  l $sldata.3, l 2, l 2,
}

data $type.1491593906 = section &quot;.data.type.1491593906&quot; {
  w 1491593906, z 4,
  l 8,
  l 4,
  w 0, z 4,
  w 3241765159, z 4,
  l $sldata.0, l 1, l 1,
  l $typeinfo.2
}

export function section &quot;.text.main&quot; &quot;ax&quot; $main() {
@start.6
	%binding.10 =l alloc8 8
@body.7
	storel $type.1491593906, %binding.10
@.8
	ret
}
</code></pre><p>The declaration of a type alias provides us with the perfect opportunity to
statically allocate a typeinfo singleton for it. Any of these which go unused by
the program are automatically stripped out by the linker thanks to the
<code>--gc-sections</code> flag. Also note that a type alias is considered a distinct
representation from the underlying struct type:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="k">type</span> <span class="n">alias</span> <span class="o">=</span> <span class="k">struct</span> <span class="p">{</span>
	<span class="n">ident</span><span class="o">:</span> <span class="p">[]</span><span class="kt">str</span><span class="p">,</span>
	<span class="n">secondary</span><span class="o">:</span> <span class="k">type</span><span class="p">,</span>
<span class="p">};</span>
</code></pre></div><p>This explains the differences in the structure of the &ldquo;type.1491593906&rdquo; global.
The <code>struct { x: int, y: int }</code> type is
the &ldquo;secondary&rdquo; field of this type.</p>
<h2 id="future-improvements">Future improvements</h2>
<p>This is just the first half of the equation. The next half is to provide useful
functions to work with this data. One such example is &ldquo;types::strenum&rdquo;:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="c1">// Returns the value of the enum at &#34;val&#34; as a string. Aborts if the value is
</span><span class="c1">// not present. Note that this does not work with enums being used as a flag
</span><span class="c1">// type, see [[strflag]] instead.
</span><span class="c1"></span><span class="k">export</span> <span class="k">fn</span> <span class="n">strenum</span><span class="p">(</span><span class="n">ty</span><span class="o">:</span> <span class="k">type</span><span class="p">,</span> <span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">void</span><span class="p">)</span> <span class="kt">str</span> <span class="o">=</span> <span class="p">{</span>
	<span class="k">const</span> <span class="n">ty</span> <span class="o">=</span> <span class="n">unwrap</span><span class="p">(</span><span class="n">ty</span><span class="p">);</span>
	<span class="k">const</span> <span class="n">en</span> <span class="o">=</span> <span class="n">ty</span><span class="p">.</span><span class="n">repr</span> <span class="n">as</span> <span class="n">enumerated</span><span class="p">;</span>
	<span class="k">const</span> <span class="n">value</span><span class="o">:</span> <span class="kt">u64</span> <span class="o">=</span> <span class="k">switch</span> <span class="p">(</span><span class="n">en</span><span class="p">.</span><span class="n">storage</span><span class="p">)</span> <span class="p">{</span>
	<span class="k">case</span> <span class="n">builtin</span><span class="o">::</span><span class="n">CHAR</span><span class="p">,</span> <span class="n">builtin</span><span class="o">::</span><span class="n">I8</span><span class="p">,</span> <span class="n">builtin</span><span class="o">::</span><span class="n">U8</span> <span class="o">=&gt;</span>
		<span class="k">yield</span> <span class="o">*</span><span class="p">(</span><span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">u8</span><span class="p">);</span>
	<span class="k">case</span> <span class="n">builtin</span><span class="o">::</span><span class="n">I16</span><span class="p">,</span> <span class="n">builtin</span><span class="o">::</span><span class="n">U16</span> <span class="o">=&gt;</span>
		<span class="k">yield</span> <span class="o">*</span><span class="p">(</span><span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">u16</span><span class="p">);</span>
	<span class="k">case</span> <span class="n">builtin</span><span class="o">::</span><span class="n">I32</span><span class="p">,</span> <span class="n">builtin</span><span class="o">::</span><span class="n">U32</span> <span class="o">=&gt;</span>
		<span class="k">yield</span> <span class="o">*</span><span class="p">(</span><span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">u32</span><span class="p">);</span>
	<span class="k">case</span> <span class="n">builtin</span><span class="o">::</span><span class="n">I64</span><span class="p">,</span> <span class="n">builtin</span><span class="o">::</span><span class="n">U64</span> <span class="o">=&gt;</span>
		<span class="k">yield</span> <span class="o">*</span><span class="p">(</span><span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">u64</span><span class="p">);</span>
	<span class="k">case</span> <span class="n">builtin</span><span class="o">::</span><span class="n">INT</span><span class="p">,</span> <span class="n">builtin</span><span class="o">::</span><span class="n">UINT</span> <span class="o">=&gt;</span>
		<span class="k">yield</span> <span class="k">switch</span> <span class="p">(</span><span class="k">size</span><span class="p">(</span><span class="kt">int</span><span class="p">))</span> <span class="p">{</span>
		<span class="k">case</span> <span class="mi">4</span> <span class="o">=&gt;</span>
			<span class="k">yield</span> <span class="o">*</span><span class="p">(</span><span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">u32</span><span class="p">);</span>
		<span class="k">case</span> <span class="mi">8</span> <span class="o">=&gt;</span>
			<span class="k">yield</span> <span class="o">*</span><span class="p">(</span><span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">u64</span><span class="p">);</span>
		<span class="k">case</span> <span class="o">=&gt;</span> <span class="k">abort</span><span class="p">();</span>
		<span class="p">};</span>
	<span class="k">case</span> <span class="n">builtin</span><span class="o">::</span><span class="n">SIZE</span> <span class="o">=&gt;</span>
		<span class="k">yield</span> <span class="k">switch</span> <span class="p">(</span><span class="k">size</span><span class="p">(</span><span class="k">size</span><span class="p">))</span> <span class="p">{</span>
		<span class="k">case</span> <span class="mi">4</span> <span class="o">=&gt;</span>
			<span class="k">yield</span> <span class="o">*</span><span class="p">(</span><span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">u32</span><span class="p">);</span>
		<span class="k">case</span> <span class="mi">8</span> <span class="o">=&gt;</span>
			<span class="k">yield</span> <span class="o">*</span><span class="p">(</span><span class="n">val</span><span class="o">:</span> <span class="o">*</span><span class="kt">u64</span><span class="p">);</span>
		<span class="k">case</span> <span class="o">=&gt;</span> <span class="k">abort</span><span class="p">();</span>
		<span class="p">};</span>
	<span class="k">case</span> <span class="o">=&gt;</span> <span class="k">abort</span><span class="p">();</span>
	<span class="p">};</span>

	<span class="k">for</span> <span class="p">(</span><span class="k">let</span> <span class="n">i</span> <span class="o">=</span> <span class="mi">0</span><span class="n">z</span><span class="p">;</span> <span class="n">i</span> <span class="o">&lt;</span> <span class="k">len</span><span class="p">(</span><span class="n">en</span><span class="p">.</span><span class="n">values</span><span class="p">);</span> <span class="n">i</span> <span class="o">+=</span> <span class="mi">1</span><span class="p">)</span> <span class="p">{</span>
		<span class="k">if</span> <span class="p">(</span><span class="n">en</span><span class="p">.</span><span class="n">values</span><span class="p">[</span><span class="n">i</span><span class="p">]</span><span class="mf">.1</span><span class="p">.</span><span class="n">u</span> <span class="o">==</span> <span class="n">value</span><span class="p">)</span> <span class="p">{</span>
			<span class="k">return</span> <span class="n">en</span><span class="p">.</span><span class="n">values</span><span class="p">[</span><span class="n">i</span><span class="p">]</span><span class="mf">.0</span><span class="p">;</span>
		<span class="p">};</span>
	<span class="p">};</span>

	<span class="k">abort</span><span class="p">(</span><span class="s">&#34;enum has invalid value&#34;</span><span class="p">);</span>
<span class="p">};</span>
</code></pre></div><p>This is used like so:</p>
<div class="highlight"><pre class="chroma"><code class="language-hare" data-lang="hare"><span class="cp">use types;</span>
<span class="cp">use fmt;</span>

<span class="k">type</span> <span class="n">watchmen</span> <span class="o">=</span> <span class="n">enum</span> <span class="p">{</span>
	<span class="n">VIMES</span><span class="p">,</span>
	<span class="n">CARROT</span><span class="p">,</span>
	<span class="n">ANGUA</span><span class="p">,</span>
	<span class="n">COLON</span><span class="p">,</span>
	<span class="n">NOBBY</span> <span class="o">=</span> <span class="o">-</span><span class="mi">1</span><span class="p">,</span>
<span class="p">};</span>

<span class="k">export</span> <span class="k">fn</span> <span class="n">main</span><span class="p">()</span> <span class="kt">void</span> <span class="o">=</span> <span class="p">{</span>
	<span class="k">let</span> <span class="n">officer</span> <span class="o">=</span> <span class="n">watchmen</span><span class="o">::</span><span class="n">ANGUA</span><span class="p">;</span>
	<span class="n">fmt</span><span class="o">::</span><span class="n">println</span><span class="p">(</span><span class="n">types</span><span class="o">::</span><span class="n">strenum</span><span class="p">(</span><span class="k">type</span><span class="p">(</span><span class="n">watchmen</span><span class="p">),</span> <span class="o">&amp;</span><span class="n">officer</span><span class="p">))</span><span class="o">!</span><span class="p">;</span> <span class="c1">// Prints ANGUA
</span><span class="c1"></span><span class="p">};</span>
</code></pre></div><p>Additional work is required to make more useful tools like this. We will
probably want to introduce a &ldquo;value&rdquo; abstraction which can store an arbitrary
value for an arbitrary type, and helper functions to assign to or read from
those values. A particularly complex case is likely to be some kind of helper
for calling a function pointer via reflection, which we I may cover in a later
article. There will also be some work to bring the &ldquo;types&rdquo; (reflection) module
closer to the <span class="redacted">xxxx</span>::* namespace, which already
features <span class="redacted">xxxx</span>::ast, <span
class="redacted">xxxx</span>::parse, and <span
class="redacted">xxxx</span>::types, so that the parser, type checker, and
reflection systems are interopable and work together to implement the <span
class="redacted">xxxx</span> type system.</p>
<hr>
<p><em>Want to help us build this language? We are primarily looking for help in the
following domains:</em></p>
<ul>
<li><em>Architectures or operating systems, to help with ports</em></li>
<li><em>Compilers &amp; language design</em></li>
<li><em>Cryptography implementations</em></li>
<li><em>Date &amp; time implementations</em></li>
<li><em>Unix</em></li>
</ul>
<p><em>If you&rsquo;re an expert in a domain which is not listed, but that you think we
should know about, then feel free to reach out. Experts are perferred, motivated
enthusiasts are acceptable. <a href="mailto:sir@cmpwn.com">Send me an email</a> if you want to help!</em></p>
