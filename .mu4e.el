;;; package --- summary
;; mu4e configuration, set up with smtpmail, and contexts for different email-addresses.

;;; Commentary:

;;; Code:
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")
(require 'mu4e)
(require 'smtpmail)
(global-set-key (kbd "C-c m") 'mu4e)
(auth-source-pass-enable)

(setq mu4e-maildir "~/Post"
      mu4e-attachment-dir "~/Post/attatchments"
      mu4e-get-mail-command "mbsync -Va"
      message-send-mail-function 'smtpmail-send-it
      mu4e-contexts
      `( ,(make-mu4e-context
           :name "gmail"
           :enter-func (lambda ()
                         (mu4e-message "Entering gmail context")
                         (when (string-match-p (buffer-name (current-buffer)) "mu4e-main")
                           (revert-buffer)))
           :leave-func (lambda ()
                         (mu4e-message "Leaving gmail context")
                         (when (string-match-p (buffer-name (current-buffer)) "mu4e-main")
                           (revert-buffer)))
           :match-func (lambda (msg)
                         (when msg
                           (or (mu4e-message-contact-field-matches msg :to "gunnar.grop@gmail.com")
                               (mu4e-message-contact-field-matches msg :from "gunnar.grop@gmail.com")
                               (mu4e-message-contact-field-matches msg :cc "gunnar.grop@gmail.com")
                               (mu4e-message-contact-field-matches msg :bcc "gunnar.grop@gmail.com")
                               (string-match-p "^/gmail/Inbox" (mu4e-message-field msg :maildir)))))
           :vars '( ( user-mail-address            . "gunnar.grop@gmail.com" )
                    ( smtpmail-smtp-user           . "gunnar.grop@gmail.com" )
                    ( mu4e-compose-signature       . "Mvh Gunnar" )
                    ( smtpmail-smtp-server         . "smtp.gmail.com" )
                    ( smtpmail-smtp-service        . 587 )
                    ( mu4e-maildir-shortcuts       . ((:maildir "/gmail/Inbox" :key ?i)))))
         ,(make-mu4e-context
           :name "mdh"
           :enter-func (lambda ()
                         (mu4e-message "Entering mdh context")
                         (when (string-match-p (buffer-name (current-buffer)) "mu4e-main")
                           (revert-buffer)))
           :leave-func (lambda ()
                         (mu4e-message "Leaving mdh context")
                         (when (string-match-p (buffer-name (current-buffer)) "mu4e-main")
                           (revert-buffer)))
           :match-func (lambda (msg)
                         (when msg
                           (or (mu4e-message-contact-field-matches msg :to "pan18010@student.mdh.se")
                               (mu4e-message-contact-field-matches msg :from "pan18010@student.mdh.se")
                               (mu4e-message-contact-field-matches msg :cc "pan18010@student.mdh.se")
                               (mu4e-message-contact-field-matches msg :bcc "pan18010@student.mdh.se"))))
           :vars '( ( user-mail-address       . "pan18010@student.mdh.se" )
                    ( smtpmail-smtp-user      . "pan18010@student.mdh.se" )
                    ( smtpmail-smtp-server    . "smtp.office365.com" )
                    ( smtpmail-smtp-service   . 587 )
		    ( smtpmail-stream-type    . starttls )
                    ( mu4e-compose-signature  . "Mvh Gunnar Andersson\npan18010" )
                    ( mu4e-maildir-shortcuts  . ((:maildir "/mdh/Inbox" :key ?i)))))))

(use-package mu4e-alert
  :ensure t
  :after mu4e
  :init
  (setq mu4e-alert-interesting-mail-query
    (concat
     "flag:unread maildir:/gmail/INBOX "
     "OR "
     "flag:unread maildir:/mdh/INBOX"
     ))
  (mu4e-alert-enable-mode-line-display)
  (defun refresh-mu4e-alert-mode-line ()
    (interactive)
    (mu4e-alert-disable-mode-line-display)
    (mu4e-alert-enable-mode-line-display))
  (run-with-timer 0 60 'refresh-mu4e-alert-mode-line))

;;; .mu4e.el ends here
