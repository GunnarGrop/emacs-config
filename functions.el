;;; package --- SUmmary

;;; Commentary:
;; Custom funtions to use ~/.emacs

;;; Code:

(defun myfunc/go-to-dotfile ()
  "Use \"find-file\" to open ~/.emacs."
  (interactive)
  (find-file "~/.emacs"))

;; Default themes
(defvar default-dark-theme 'doom-gruvbox)
(defvar default-light-theme 'doom-gruvbox-light)
(defun myfunc/toggle-theme ()
  "Toggle between two predefined themes."
  (interactive)
  (cond ((memq default-dark-theme custom-enabled-themes)
	 (disable-theme default-dark-theme)
	 (load-theme default-light-theme))
	((memq default-light-theme custom-enabled-themes)
	 (disable-theme default-light-theme)
	 (load-theme default-dark-theme))))

(defun myfunc/switch-dictionary()
  "Change the ispell-currect-dictionary between english and swedish."
  (interactive)
  (let* ((dic ispell-current-dictionary)
    	 (change (if (string= dic "svenska") "english" "svenska")))
    (ispell-change-dictionary change)
    (message "Dictionary switched from %s to %s" dic change)))

(defun myfunc/linux-c-c++-mode()
  "Hook this func to C/C++ mode."
  (interactive)
  (c-set-style "linux")
  (setq c-basic-offset 4))

(defun colorize-compilation-buffer ()
  "Use this to colorize the compilation-buffer."
  (read-only-mode)
  (ansi-color-apply-on-region compilation-filter-start (point))
  (read-only-mode))

(provide 'functions)
;;; functions.el ends here
